#!/bin/bash

./generate_table.rb \
metagenomic-mapping-rates.txt \
metagenomic-pairing-rates.txt \
metagenomic-wallclock-times.txt \
sums.txt n50.txt \
"metagenomic reads" \
> metagenomic-table.html

./generate_table.rb \
metatranscriptomic-mapping-rates.txt \
metatranscriptomic-pairing-rates.txt \
metatranscriptomic-wallclock-times.txt \
sums.txt n50.txt \
"metatranscriptomic reads" \
> metatranscriptomic-table.html
