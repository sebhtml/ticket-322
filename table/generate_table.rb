#!/usr/bin/env ruby

require 'set'

class Generator

    def initialize mapping_file, pairing_file, wallclock_file, sum_file, n50_file, title
        @parameters = Hash.new
        @values = Hash.new

        load_mapping mapping_file
        load_pairing pairing_file
        load_wallclock wallclock_file
        load_sum sum_file
        load_n50 n50_file

#puts @parameters
#puts @values
        generate_report title
    end

    def load_n50 file
        File.open(file).each do |line|
            tokens = line.split
            parameters = tokens.first.gsub "outputs/ROBOT:", ""
            keys = Hash.new
            parameters.gsub('/', ' ').split.first.split(',').each do |token|
                key, value = token.split '='
                keys[key] = value
            end

            add_keys keys
            value = tokens.last.strip
            real_keys = Hash.new

            real_keys["SAMPLE"] = keys["SAMPLE"]
            real_keys["ASSEMBLER"] = keys["ASSEMBLER"]
            real_keys["THRESHOLD"] = keys["THRESHOLD"]
            real_keys["METRIC"] = "n50"
            add_value real_keys, value
        end

    end

    def load_sum file
        File.open(file).each do |line|
            tokens = line.split
            parameters = tokens.first.gsub "outputs/ROBOT:", ""
            keys = Hash.new
            parameters.gsub('/', ' ').split.first.split(',').each do |token|
                key, value = token.split '='
                keys[key] = value
            end

            add_keys keys
            value = tokens.last.strip
            real_keys = Hash.new

            real_keys["SAMPLE"] = keys["SAMPLE"]
            real_keys["ASSEMBLER"] = keys["ASSEMBLER"]
            real_keys["THRESHOLD"] = keys["THRESHOLD"]
            real_keys["METRIC"] = "sum"
            add_value real_keys, value
        end
    end

    def load_mapping file
# 
# outputs/ROBOT:SAMPLE=BW4granules,ASSEMBLER=idba,THRESHOLD=1000,THREADS=24,ALIGNER=bowtie2-2.1.0/bowtie2-2.1.0.log:8156311 + 0 mapped (83.29%:-nan%)
#
        File.open(file).each do |line|
            tokens = line.split
            parameters = tokens.first.gsub "outputs/ROBOT:", ""
            keys = Hash.new
            keys["METRIC"] = "mapped"
            parameters.gsub('/', ' ').split.first.split(',').each do |token|
                key, value = token.split '='
                keys[key] = value
            end

            add_keys keys
            value = tokens.last.split(':').first.gsub('(', '')

            add_value keys, value
        end
    end

    def add_value keys, value
        key = keys['SAMPLE'].to_s + keys['ASSEMBLER'].to_s + keys['THRESHOLD'].to_s + keys['ALIGNER'].to_s + keys['METRIC'].to_s

=begin
        if keys["METRIC"] == "sum"
            puts key
        end
=end

        @values[key] = value
    end

    def get_value keys
        @values[keys['SAMPLE'].to_s + keys['ASSEMBLER'].to_s + keys['THRESHOLD'].to_s + keys['ALIGNER'].to_s + keys['METRIC'].to_s]
    end

    def add_keys keys
        keys.each do |key, value|

            unless @parameters.has_key? key
                @parameters[key] = Set.new
            end

            @parameters[key].add value
        end
    end

    def load_pairing file
# outputs/ROBOT:SAMPLE=BW4granules,ASSEMBLER=idba,THRESHOLD=1000,THREADS=24,ALIGNER=bowtie2-2.1.0/bowtie2-2.1.0.log:5909402 + 0 properly paired (60.34%:-nan%)
#
        File.open(file).each do |line|
            tokens = line.split
            parameters = tokens.first.gsub "outputs/ROBOT:", ""
            keys = Hash.new
            keys["METRIC"] = "paired"
            parameters.gsub('/', ' ').split.first.split(',').each do |token|
                key, value = token.split '='
                keys[key] = value

            end

            add_keys keys
            value = tokens.last.split(':').first.gsub('(', '')

            add_value keys, value
        end

    end

    def load_wallclock file
# ROBOT:SAMPLE=BW4granules,ASSEMBLER=idba,THRESHOLD=1000,THREADS=24,ALIGNER=bowtie2-2.1.0.o2718040: #         cput=00:00:00,mem=5636kb,vmem=130540kb,walltime=02:58:10         #
#
        File.open(file).each do |line|
            tokens = line.split
            parameters = tokens.first.gsub "ROBOT:", ""
            keys = Hash.new
            keys["METRIC"] = "walltime"
            parameters.gsub('.o', ' ').split.first.split(',').each do |token|
                key, value = token.split '='
                keys[key] = value

            end

            add_keys keys
            value = tokens.at(tokens.size - 2).split(',').last.split('=').last

            add_value keys, value
        end
    end

    def generate_report title
        @parameters['SAMPLE'].each do |sample|
            puts "<html><body><table border='5'><caption>" + sample + ", group: " + title + "</caption><tbody>"
            puts "<tr><th rowspan='2'>Assembler</th><th rowspan='2'>Threshold (nucleotides)</th>"
            puts "<th rowspan='2'>Sum (nucleotides)</th>"
            puts "<th rowspan='2'>N50 (nucleotides)</th>"

            @parameters['ALIGNER'].each do |aligner|
                puts "<th colspan=\"3\">" + aligner + "</th>"
            end
            puts "</tr>"
            puts "<tr>"

            @parameters['ALIGNER'].each do |aligner|
                @parameters['METRIC'].each do |metric|
                    puts "<th>" + metric + "</th>"
                end
            end
            puts "</tr>"

            @parameters['ASSEMBLER'].each do |assembler|
                @parameters['THRESHOLD'].to_a.collect{|i| i.to_i}.sort.each do |threshold|
                    puts "<tr><td>" + assembler + "</td><td>" + threshold.to_s + "</td>"

                    keys = Hash.new
                    keys['SAMPLE'] = sample
                    keys['ASSEMBLER'] = assembler
                    keys['THRESHOLD'] = threshold
                    keys['METRIC'] = "sum"
                    value = get_value keys

                    puts "<td>" + value + "</td>"

                    keys = Hash.new
                    keys['SAMPLE'] = sample
                    keys['ASSEMBLER'] = assembler
                    keys['THRESHOLD'] = threshold
                    keys['METRIC'] = "n50"
                    value = get_value keys

                    puts "<td>" + value+ "</td>"

                    @parameters['ALIGNER'].each do |aligner|
                        @parameters['METRIC'].each do |metric|
                            @parameters['THREADS'].each do |threads|
                                keys = Hash.new
                                keys['SAMPLE'] = sample
                                keys['ASSEMBLER'] = assembler
                                keys['METRIC'] = metric
                                keys['ALIGNER'] = aligner
                                keys['THRESHOLD'] = threshold.to_s
                                keys['THREADS'] = threads

                                value = get_value keys
                                puts "<td>" + value + "</td>"
                            end
                        end
                    end

                    puts "</tr>"
                end
            end
            puts "</table></body></html>"
        end
    end
end

Generator.new ARGV[0], ARGV[1], ARGV[2], ARGV[3], ARGV[4], ARGV[5]
