#!/bin/bash

echo "DEBUG loading apps from Beagle storage system..."

source /opt/modules/3.2.6.7/init/bash
module swap PrgEnv-cray PrgEnv-gnu
module load bwa/0.7.4
module load samtools/0.1.19
module load bowtie2/2.1.0
module load python/2.7.1

echo "DEBUG successfully loaded apps !"

function validate_file()
{
    local file=$1

    if ! test -f $file
    then
        echo "Error file $file does not exist"
        exit
    fi
}

function align_bwa()
{
    local aligner=$1
    local reference=$2
    local threads=$3
    local reads1=$4
    local reads2=$5
    local bam_file

    bam_file="$aligner.bam"

    echo "DEBUG index"
    time bwa index $reference
    echo "DEBUG align"
    echo "time bwa mem -t $threads $reference $reads1 $reads2"
    time bwa mem -t $threads $reference $reads1 $reads2 | samtools view -bS - > $bam_file
    echo "DEBUG statistics"
    time samtools flagstat $bam_file
    rm $bam_file
    rm $reference.*

}

function align_bowtie2()
{
    local aligner=$1
    local reference=$2
    local threads=$3
    local reads1=$4
    local reads2=$5
    local bam_file

    bam_file="$aligner.bam"

    echo "DEBUG index"
    time bowtie2-build $reference $reference

    echo "DEBUG align"
    # XXX somehow, bowtie2 and samtools do not like each other, piping causes a hang
    #time bowtie2 -p $threads -x $reference -1 $reads1 -2 $reads2 | samtools view -bS - > $bam_file
    time bowtie2 -p $threads -x $reference -1 $reads1 -2 $reads2 > 1.sam
    samtools view -bS 1.sam > $bam_file
    rm 1.sam

    echo "DEBUG statistics"
    time samtools flagstat $bam_file
    rm $bam_file
    rm $reference.*
}

function align()
{
    local reference=$1
    local threshold=$2
    local reads1=$3
    local reads2=$4
    local output=$5
    local threads=$6
    local aligner=$7
    local new_reference

    echo "DEBUG align() $reference $threshold $reads1 $reads2 $output $threads $aligner"

    # go in the workspace
    cd "outputs/$output"

    # add the symbolic link for the reference.
    ln -s ../../$reference reference.fasta

    # add links for reads
    ln -s ../../$reads1 reads_1.fastq.gz
    ln -s ../../$reads2 reads_2.fastq.gz

    # create the filtered version
    new_reference="reference-THRESHOLD=$threshold.fasta"
    ../../apply_threshold.pl $threshold reference.fasta > $new_reference
    ../../get_statistics.py $new_reference

    if test $aligner = "bwa-0.7.4"
    then
        align_bwa $aligner $new_reference $threads reads_1.fastq.gz reads_2.fastq.gz
    fi

    if test $aligner = "bowtie2-2.1.0"
    then
        align_bowtie2 $aligner $new_reference $threads reads_1.fastq.gz reads_2.fastq.gz
    fi

    cd ../.. # go back
}

function main()
{
    local sample
    local group
    local assembler
    local threshold
    local threads
    local assembly
    local reads1
    local reads2
    local output
    local aligner

    sample=$1
    group=$2
    assembler=$3
    threshold=$4
    threads=$5
    aligner=$6

    echo "DEBUG main() $sample $assembler $threshold $threads $aligner"

    assembly="$sample-assembly-$assembler/assembly.fasta"
    reads1="$sample-$group-reads/reads_1.fastq.gz"
    reads2="$sample-$group-reads/reads_2.fastq.gz"
    output="ROBOT:SAMPLE=$sample,GROUP=$group,ASSEMBLER=$assembler,THRESHOLD=$threshold,THREADS=$threads,ALIGNER=$aligner"

    validate_file $assembly
    validate_file $reads1
    validate_file $reads2

    mkdir -p "outputs/$output"

    align $assembly $threshold $reads1 $reads2 $output $threads $aligner &> "outputs/$output/log.txt"
}

main $@
