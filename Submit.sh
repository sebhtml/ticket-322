#!/bin/bash

function main()
{
	local job

	for job in $(ls robots/*.pbs|grep pbs|grep ROBOT)
	do
		qsub $job &> $job.job_number
	done
}

main $@
