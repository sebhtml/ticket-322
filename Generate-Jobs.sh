#!/bin/bash

function generate_sample()
{
	local sample
    local group
	local assembler
	local threshold
	local job
	local expression
	local threads

	threads=$(cat threads.txt)
	sample=$1
    group=$2

	for assembler in $(cat assemblers.txt)
	do

		for threshold in $(cat thresholds.txt)
		do
			for aligner in $(cat aligners.txt)
			do
				job="robots/ROBOT:SAMPLE=$sample,GROUP=$group,ASSEMBLER=$assembler,THRESHOLD=$threshold,THREADS=$threads,ALIGNER=$aligner.pbs"
				cp "Job-template.pbs" $job

				expression="s/_SAMPLE_/$sample/g"
				sed -i $expression $job

				expression="s/_GROUP_/$group/g"
				sed -i $expression $job

				expression="s/_ALIGNER_/$aligner/g"
				sed -i $expression $job

				expression="s/_ASSEMBLER_/$assembler/g"
				sed -i $expression $job

				expression="s/_THRESHOLD_/$threshold/g"
				sed -i $expression $job
	
				expression="s/_THREADS_/$threads/g"
				sed -i $expression $job
			done
		done
	done
}

function generate_samples()
{
	local sample

	for sample in $(cat samples.txt)
	do
        for group in $(cat groups.txt)
        do
		    generate_sample $sample $group
        done
	done
}

function main()
{
	generate_samples
}

main
