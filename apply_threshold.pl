#!/usr/bin/env perl
use strict;
use warnings;

## removesmalls.pl
## https://www.biostars.org/p/79202/
## Author: Vivek Krishnakumar

my $minlen = shift or die "Error: `minlen` parameter not provided\n";
{
    local $/=">";
    while(<>) {
        chomp;
        next unless /\w/;
        s/>$//gs;
        my @chunk = split /\n/;
        my $header = shift @chunk;
        my $seqlen = length join "", @chunk;
        print ">$_" if($seqlen >= $minlen);
    }
    local $/="\n";
}
