#!/usr/bin/env python

import sys

def get_sum(lengths):
    sum = 0
    for length in lengths:
        sum += length
    return sum

def get_n50(lengths):
    sum = get_sum(lengths)
    sorted_lengths = sorted(lengths, reverse = True)

    #print(sorted_lengths)
    sum2 = 0
    half_sum = sum / 2.0

    for length in sorted_lengths:
        sum2 += length

        if sum2 >= half_sum:
            return length

def main():
    argc = len(sys.argv)

    if argc == 1:
        return
    file = sys.argv[1]
    lengths = []
    headers = []

    header = ""
    has_header = 0
    current_length = 0

    for line in open(file):
        if len(line.strip()) > 0 and line[0] == '>':
            if has_header:
                lengths.append(current_length)
                headers.append(header)
                current_length = 0
            has_header = 1
            header = line.strip()
        else:
            length = len(line.strip())
            current_length += length

    if has_header:
        lengths.append(current_length)
        headers.append(header)

    n50 = get_n50(lengths)
    sum = get_sum(lengths)

    print("N50: " + str(n50))
    print("SUM: " + str(sum))

"""
    i = 0

    while i < len(headers):
        header = headers[i]
        length = lengths[i]
        #print("#" + str(i) + " " + header + " " + str(length))
        i += 1
"""

if __name__ == "__main__":
    main()
